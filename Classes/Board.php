<?php

class Board {

	var $stonesOnTable = [];

	public function __construct($stone) {
		array_push($this->stonesOnTable, $stone);
	}

	public function getCurrentTable() {
		$outputString = "";

		foreach ($this->stonesOnTable as $stone) {
			$outputString .= "<".$stone->getValue()[0].":".$stone->getValue()[1]."> ";
		}

		return $outputString;
	}

	public function putStoneOnTablesLeftSide($stone) {
		array_unshift($this->stonesOnTable, $stone);
	}

	public function getStoneOnTablesLeftSide() {
		return $this->stonesOnTable[0];
	}

	public function putStoneOnTablesRightSide($stone) {
		array_push($this->stonesOnTable, $stone);
	}

	public function getStoneOnTablesRightSide() {
		return $this->stonesOnTable[count($this->stonesOnTable) - 1];
	}

}