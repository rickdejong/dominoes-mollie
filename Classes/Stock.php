<?php

class Stock {

	var $stock = [];

	public function __construct() {
		for ($firstSide = 0; $firstSide <= 6; $firstSide++) {
			for ($secondSide = 0; $secondSide <= $firstSide; $secondSide++) {
				$stone = new Stone([$firstSide, $secondSide]);
				array_push($this->stock, $stone);
			}
		}

		shuffle($this->stock);
	}

	public function stonesLeft() {
		return count($this->stock);
	}

	public function drawStone() {
		$drawedStone = array_shift($this->stock);

		return $drawedStone;
	}

	public function getStock() {
		return $this->stock;
	}

}