<?php

class Player {

	var $name = "";
	var $currentHand = [];

	public function __construct($name) {
		$this->name = $name;
	}

	public function getName() {
		return $this->name;
	}

	public function setHand($hand) {
		$this->currentHand = $hand;
	}

	public function getHand() {
		return $this->currentHand;
	}

	public function removeStoneFromHand($index) {
		unset($this->currentHand[$index]);
		$this->currentHand = array_values($this->currentHand);
	}

	public function numberOfStonesInHand() {
		return count($this->currentHand);
	}

	public function addStoneToHand($stone) {
		array_push($this->currentHand, $stone);
	}

	public function getTotalStoneEyesInHand() {
		$eyes = 0;
		foreach ($this->currentHand as $stone) {
			$eyes += ($stone->getValue()[0] + $stone->getValue()[1]);
		}

		return $eyes;
	}

	public function printHand() {
		$hand = "";
		foreach ($this->currentHand as $stone) {
			$hand .= "<".$stone->getValue()[0].":".$stone->getValue()[1]."> ";
		}

		return $hand;
	}

}