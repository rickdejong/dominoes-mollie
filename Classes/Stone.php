<?php

class Stone {

	var $firstSide = 0;
	var $secondSide = 0;

	public function __construct($values) {
		$this->firstSide = $values[0];
		$this->secondSide = $values[1];
	}

	public function getValue() {
		return [$this->firstSide, $this->secondSide];
	}

	public function printStone() {
		return "<".$this->firstSide.":".$this->secondSide.">";
	}

	public function rotateStone() {
		$tmp = $this->firstSide;
		$this->firstSide = $this->secondSide;
		$this->secondSide = $tmp;
	}

}