<?php

// Import classes that we need
include('Classes/Player.php');
include('Classes/Stone.php');
include('Classes/Stock.php');
include('Classes/Board.php');

// Settings
$numberOfStoneToDrawAtStart = 7;
$currentTurn = 0; // 0 = Player 1 starts, 1 = Player 2 starts

// Lets start a game by creating the players, adding them to the players array, and spread the word!
$playerOne = new Player('Alice');
$playerTwo = new Player('Bob');
$playersInGame = [$playerOne, $playerTwo];
echo "Today players are <b>".$playersInGame[0]->getName()."</b> and <b>".$playersInGame[1]->getName()."</b>. Who will win?<br>";

// Create the stock of stones and the playing board
$stock = new Stock();

// Each player draws their stones to start
foreach ($playersInGame as $player) {
	for ($i = 0; $i < $numberOfStoneToDrawAtStart; $i++) {
		$player->addStoneToHand($stock->drawStone());
	}
}

// Create table and put first stone on it and show it to the audience
$board = new Board($stock->drawStone());
echo "Game starting with the first tile: <b>".$board->getCurrentTable()."</b><br>";

$skippedTurns = 0;

// Play the game
do {

	// See which player's turn it is.
	$currentPlayer = $playersInGame[$currentTurn % 2];

	// Get current stones on each side of the table
	$leftStone = $board->getStoneOnTablesLeftSide();
	$rightStone = $board->getStoneOnTablesRightSide();

	// Variables we use here
	$stonePlayed = false;
	$stoneIndexInHand = 0;

	foreach ($currentPlayer->getHand() as $stone) {
		// Have this player played already?
		if (!$stonePlayed) {
			// Check first left stone to see if the player can play a stone
			if (in_array($leftStone->getValue()[0], $stone->getValue())) {
				// The player found a stone to play! Lets see if we need to turn the stone around
				if ($leftStone->getValue()[0] != $stone->getValue()[1]) {
					// Okay, lets rotate!
					$stone->rotateStone();
				}
				
				// Put the stone on table
				$board->putStoneOnTablesLeftSide($stone);

				// Remove stone from hand of player
				$currentPlayer->removeStoneFromHand($stoneIndexInHand);

				// Spread the word
				echo $currentPlayer->getName()." plays ".$stone->printStone()." to connect to tile ".$leftStone->printStone()." on the board.<br>";
				echo "Board is now: ".$board->getCurrentTable()."<br>";

				// Set that we have played
				$stonePlayed = true;
				$currentTurn++;
				$skippedTurns = 0;
			} else if (in_array($rightStone->getValue()[1], $stone->getValue())) {
				// Okay, the player can't play on that side with this stone, lets try the right
				// The player found a stone to play! Lets see if we need to turn the stone around
				if ($rightStone->getValue()[1] != $stone->getValue()[0]) {
					// Okay, lets rotate!
					$stone->rotateStone();
				}
				
				// Put the stone on table
				$board->putStoneOnTablesRightSide($stone);

				// Remove stone from hand of player
				$currentPlayer->removeStoneFromHand($stoneIndexInHand);

				// Spread the word
				echo $currentPlayer->getName()." plays ".$stone->printStone()." to connect to tile ".$leftStone->printStone()." on the board.<br>";
				echo "Board is now: ".$board->getCurrentTable()."<br>";

				// Set that we have played and change turns
				$stonePlayed = true;
				$currentTurn++;
				$skippedTurns = 0;
			}

			// Check the other stone 
			$stoneIndexInHand++;
		}
	}

	// Just to be sure when the loop is too fast.
	if ($currentPlayer->numberOfStonesInHand() == 0) {
		// Stop loop
		break;
	}

	// All stones checked, lets see if the player is allowed to draw.
	if (!$stonePlayed && $stock->stonesLeft() != 0) {
		// Draw a stone and add it to the players hand.
		$stoneDrawed = $stock->drawStone();
		$currentPlayer->addStoneToHand($stoneDrawed);

		echo $currentPlayer->getName()." can't play, drawing tile ".$stoneDrawed->printStone()."<br>";
	} else if ($stock->stonesLeft() == 0 && $skippedTurns != 2) {
		echo $currentPlayer->getName()." can't play but the stock is empty. Change turns.<br>";
		$currentTurn++;
		$skippedTurns++;
	} else if ($skippedTurns == 2) {
		// All players tries to draw. Stop loop, just to be sure.
		break;
	}

} while ($playersInGame[0]->numberOfStonesInHand() != 0 || $playersInGame[1]->numberOfStonesInHand() != 0);

// Get the player who won and show it!
if ($playersInGame[0]->numberOfStonesInHand() == 0 || $playersInGame[1]->numberOfStonesInHand() == 0) {
	$winner = $playersInGame[($currentTurn - 1) % 2];
	echo "Player ".$winner->getName()." has won!";
} else {
	// Check the amount of eyes in hand. The player with the lowest wins
	echo "All player can't play anymore. The player with the lowest eyes in his/her hand wins.<br>";
	foreach ($playersInGame as $player) {
		echo "Player ".$player->getName()." having ".$player->getTotalStoneEyesInHand()." eyes in his/her hand: ".$player->printHand()."<br>";
	}
	if ($playersInGame[0]->getTotalStoneEyesInHand() > $playersInGame[1]->getTotalStoneEyesInHand()) {
		echo $playersInGame[1]->getName()." has won!";
	} else if ($playersInGame[0]->getTotalStoneEyesInHand() == $playersInGame[1]->getTotalStoneEyesInHand()) {
		echo "It is a tie.";
	} else {
		echo $playersInGame[0]->getName()." has won!";
	}
}